//
//  ViewController.swift
//  CustomSegueAnimation
//
//  Created by iOS_Razrab on 06/10/2017.
//  Copyright © 2017 iOS_Razrab. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBAction func prepareForUnwind (segue: UIStoryboardSegue) {
        
    }
    
    override func unwind(for unwindSegue: UIStoryboardSegue, towardsViewController subsequentVC: UIViewController) {
        let segue = UnwindScaleSegue(identifier: unwindSegue.identifier, source: unwindSegue.source, destination: unwindSegue.destination)
        segue.perform()
    }


}

